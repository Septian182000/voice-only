import logo from "./logo.svg";
import "./App.css";
import { useState } from "react";
import MicRecorder from "mic-recorder-to-mp3";
import { createDAHUAAudioRequest } from "./createDAHUAAudioRequest";
import icMicOn from "../src/assets/icon/ic_voice_on.png";
import icMicOff from "../src/assets/icon/ic_voice_off.png";
import icMicCircle from "../src/assets/icon/ic_mic_circle.png";
import icMicOnAnimate from "../src/assets/icon/micAnimate.json";
import Lottie from "lottie-react";

function App() {
  const [micHold, setMicHold] = useState();
  const [recorderVoice, setRecorderVoice] = useState(
    new MicRecorder({
      bitRate: 128,
    })
  );

  const startRecord = () => {
    recorderVoice
      .start()
      .then((data) => {
        // console.log(data);
        // something else
      })
      .catch((e) => {
        console.error(e);
      });
  };

  const stopRecord = () => {
    recorderVoice
      .stop()
      .getMp3()
      .then(([buffer, blob]) => {
        // do what ever you want with buffer and blob
        // Example: Create a mp3 file and play
        const file = new File(buffer, "me-at-thevoice.mp3", {
          type: blob.type,
          lastModified: Date.now(),
        });

        // const formData = new FormData();
        // formData.append("postAudio", file);

        createDAHUAAudioRequest(file);

        // dispatch(voiceCall({ voice: file }));

        // const downloadLink = document.createElement("a");
        // downloadLink.href = URL.createObjectURL(file);
        // downloadLink.download = "recording.mp3";
        // downloadLink.click();

        // const player = new Audio(URL.createObjectURL(file));
        // player.play();
      })
      .catch((e) => {
        alert("We could not retrieve your message");
        console.log(e);
      });
  };
  //
  return (
    <div className="App">
      {!micHold ? (
        <img
          src={icMicCircle}
          alt=""
          style={{ height: 50, cursor: "pointer ", marginTop: 100 }}
          onMouseDown={() => {
            setMicHold(true);
            startRecord();
          }}
        />
      ) : (
        <Lottie
          animationData={icMicOnAnimate}
          loop={true}
          style={{
            height: 120,
            cursor: "pointer ",
            marginTop: 65,
            marginRight: 0,
          }}
          onMouseUp={() => {
            setMicHold(false);
            stopRecord();
          }}
          onMouseLeave={() => {
            setMicHold(false);
            stopRecord();
          }}
        />
      )}
    </div>
  );
}

export default App;
